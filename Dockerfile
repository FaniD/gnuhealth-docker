ARG OPENSUSE_LEAP_VERSION
FROM opensuse/leap:${OPENSUSE_LEAP_VERSION}

RUN zypper up -y && zypper install -y \
	gnuhealth \
	gnuhealth-client \
	mygnuhealth \
	mygnuhealth-doc \
	python3-PyWebDAV3-GNUHealth \
	gnuhealth-thalamus \
	gnuhealth-orthanc

VOLUME /var/lib/tryton

ARG DUMPFILE
ARG SQLFILE

VOLUME /var/lib/postgresql/data

RUN zypper install -y \
	gzip curl ca-certificates \
	&& curl -o /$DUMPFILE -SL "http://health.gnu.org/downloads/postgres_dumps/$DUMPFILE" \
        && gzip -d $DUMPFILE \
	&& mv ${SQLFILE} demo.sql

USER tryton
COPY trytond.conf /etc/tryton/trytond.conf
COPY gnuhealth.sh /gnuhealth.sh
RUN chmod +x /gnuhealth.sh
ENTRYPOINT ["/gnuhealth.sh"]
